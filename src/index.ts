import 'dotenv/config';
import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import moment from 'moment';
import http from 'http';

console.clear();
const { NODE_ENV, HOST, PORT } = process.env;

const isDevEnv = NODE_ENV !== 'production';
const host = HOST || '0.0.0.0';
const port = Number(PORT) || 8080;

const app = express();
const server = http.createServer(app);
const router = express.Router();

if (isDevEnv) app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
  morgan.token('date', () => moment().format('YYYY-MM-DD HH:mm:ss.SSS'))(
    `[:date[clf]] :remote-addr - :remote-user ":method :url HTTP/:http-version" :status :res[content-length] ":referrer"`
  )
);
app.use('/api', router);

router.all('/git', (req, res) => {
  const { headers, body } = req;
  console.log(JSON.stringify({ headers, body }, null, 2));
  res.sendStatus(200);
});

server.listen({ host, port }, () => console.log(`> Running on port ${port}`));
