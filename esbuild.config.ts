import esbuild from 'esbuild';
import { nodeExternalsPlugin } from 'esbuild-node-externals';
import path from 'path';

(async () => {
  try {
    await esbuild.build({
      entryPoints: ['src/index.ts'],
      bundle: true,
      minify: true,
      platform: 'node',
      outfile: 'dist/index.js',
      alias: {
        '@': path.resolve(process.cwd(), 'src')
      },
      plugins: [nodeExternalsPlugin()]
    });

    process.exit();
  } catch (err) {
    console.error(err);
    process.exit(-1);
  }
})();
